/*
 * WP Quiz Pro Plugin by MyThemeShop
 * https://mythemeshop.com/plugins/wp-quiz-pro/
*/
;(function ($, window, document, undefined) {
	var pluginName = "jTinder",
		defaults = {
			onDislike: null,
			onLike: null,
			likeSelector: '.like',
			dislikeSelector: '.dislike',
			perspective: 1000,
			perspectiveOrigin : '50% -50%',
			visible: 3,
			infinite : true,
			stackItemsAnimationDelay : 500,
			stackItemsAnimation : {
				duration : 500,
				type : dynamics.bezier,
				points : [{'x':0,'y':0,'cp':[{'x':0.25,'y':0.1}]},{'x':1,'y':1,'cp':[{'x':0.25,'y':1}]}]
			}
		},
		$that = null,
		hammer_instance = null;

	function Plugin(element, options) {
		this.element = element;
		this.settings = $.extend({}, defaults, options);
		this._defaults = defaults;
	
		this.items = [].slice.call(this.element.children[0].children);
		this.itemsTotal = this.items.length;
		if( this.settings.infinite && this.settings.visible >= this.itemsTotal || !this.settings.infinite && this.settings.visible > this.itemsTotal || this.settings.visible <=0 ) {
			this.settings.visible = 1;
		}
		
		$that = this;
		this.current = 0;
		this.init(element);
	}

	Plugin.prototype = {

		init: function (element) {
			this.element.children[0].style.WebkitPerspective = this.element.children[0].style.perspective = this.settings.perspective + 'px';
			this.element.children[0].style.WebkitPerspectiveOrigin = this.element.children[0].style.perspectiveOrigin = this.settings.perspectiveOrigin;
		
			// the items
			for(var i = 0; i < this.itemsTotal; ++i) {
				var item = this.items[i];
				if( i < this.settings.visible ) {
					item.style.opacity = 1;
					item.style.pointerEvents = 'auto';
					item.style.zIndex = i === 0 ? parseInt(this.settings.visible + 1) : parseInt(this.settings.visible - i);
					item.style.WebkitTransform = item.style.transform = 'translate3d(0px, 0px, ' + parseInt(-1 * 50 * i) + 'px)';
				}
				else {
					item.style.WebkitTransform = item.style.transform = 'translate3d(0,0,-' + parseInt(this.settings.visible * 50) + 'px)';
				}
			}
			$(this.items[this.current]).addClass('activeSlide');
			$that.init_swipe(element);
		},

		init_swipe: function(element){
			current_pane = this.items[this.current];
			hammer_instance = new Hammer(current_pane);
			hammer_instance.on('pan panend', this.handler);
		},
		
		next: function () {
			if( this.isAnimating || ( !this.settings.infinite && this.hasEnded ) ) return;
			this.isAnimating = true;
			
			// current item
			var currentItem = this.items[this.current];
			$(currentItem).removeClass('activeSlide');
		
			setTimeout(function(){
				// reset current item
				currentItem.style.opacity = 0;
				currentItem.style.pointerEvents = 'none';
				currentItem.style.zIndex = -1;
				currentItem.style.WebkitTransform = currentItem.style.transform = 'translate3d(0px, 0px, -' + parseInt($that.settings.visible * 50) + 'px)';
				$(currentItem).removeClass("elasticBackOut showDislike showLike");

				$that.items[$that.current].style.zIndex = $that.settings.visible + 1;
				$that.isAnimating = false;
				
				if( !$that.settings.infinite && $that.current === 0 ) {
					$that.hasEnded = true;
					// callback
					$that.settings.onEndStack($that,$that.element);
				}
			},400);
			
			// set style for the other items
			for(var i = 0; i < this.itemsTotal; ++i) {
				if( i >= this.settings.visible ) break;

				if( !this.settings.infinite ) {
					if( this.current + i >= this.itemsTotal - 1 ) break;
					var pos = this.current + i + 1;
				}
				else {
					var pos = this.current + i < this.itemsTotal - 1 ? this.current + i + 1 : i - (this.itemsTotal - this.current - 1);
				}

				var item = this.items[pos],
					// stack items animation
					animateStackItems = function(item, i) {
						item.style.pointerEvents = 'auto';
						item.style.opacity = 1;
						item.style.zIndex = parseInt($that.settings.visible - i);
					
						dynamics.animate(item, {
							translateZ : parseInt(-1 * 50 * i)
						}, $that.settings.stackItemsAnimation);
					};

				setTimeout(function(item,i) {
					return function() {
						animateStackItems(item, i);
					};
				}(item,i), this.settings.stackItemsAnimationDelay);
			}
			// update current
			this.current = this.current < this.itemsTotal - 1 ? this.current + 1 : 0;
			$(this.items[this.current]).addClass('activeSlide');
			hammer_instance.off('pan panend');
		},

		onEndAnimation:  function( el, callback ) {
			var animEndEventName = 'transitionend';
			var onEndCallbackFn = function( ev ) {
				//if( support.animations ) {
					if( ev.target != this ) return;
					this.removeEventListener( animEndEventName, onEndCallbackFn );
				//}
				if( callback && typeof callback === 'function' ) { callback.call(); }
			};
			
			el.addEventListener( animEndEventName, onEndCallbackFn );
			
		},
		
		dislike: function() {
			$that.removeFromDeck(current_pane,-80);	
			$that.next();
			$that.init_swipe($that.element);
			
		},

		like: function() {
			$that.removeFromDeck(current_pane,80);		
			$that.next();
			$that.init_swipe($that.element);
		},
		
		restart: function(){
			this.hasEnded = false;
			this.init();
		},
		
		handler: function (ev) {
			ev.preventDefault();

			switch (ev.type) {
				case 'pan':
					if(ev.deltaX > 20|| ev.deltaX < -20){
						$(current_pane).removeClass("elasticBack");
						$(current_pane).toggleClass("showDislike",ev.deltaX<0);
						$(current_pane).toggleClass("showLike",ev.deltaX>0);
						$that.cardTransform(current_pane,ev.deltaX,0);
						
					}
					break;
				case 'panend':
					if(Math.abs(ev.deltaX)<100){
						$(current_pane).addClass("elasticBack"),
						$(current_pane).removeClass("showDislike showLike"),
						$that.cardTransform(current_pane,0,0);
					}else{
						$that.removeFromDeck(current_pane,ev.deltaX);
						$that.next();
						$that.init_swipe($that.element);
					}
					break;
			}
		},
		
		cardTransform: function( ele, deltaX){
			var trans = "translate("+deltaX+"px, "+Math.abs(deltaX/5)+"px) rotate("+$that.calculateRotation(deltaX,100,ele)+"deg)";
			$that.addTransformStyle(trans,ele);
		},
		
		calculateRotation: function(deltaX,t,ele){
			var r=Math.min(Math.max(deltaX/ele.offsetWidth,-1),1),
				u=(t>0?1:-1)*Math.min(Math.abs(t)/100,1);
			return r*u*25;
		},
		
		addTransformStyle: function(trans,ele){
			if(trans.length){
				trans="translate3d(0, 0, 0) "+trans;
			}
			$.each(["-webkit-transform","-moz-transform","-ms-transform","transform"],function(i,v){
				if(ele && ele.style){
					ele.style[v]=trans;
				}
			})
		},
		
		removeFromDeck: function(ele,deltaX){
			if(deltaX>0){
				if($that.settings.onLike) {
					$that.settings.onLike(ele,$that.element);
				}
			}else{
				if($that.settings.onDislike) {
					$that.settings.onDislike(ele,$that.element);
				}
			}
			$(ele).addClass("elasticBackOut");
			$that.cardTransform(ele,deltaX*10,0)
		}
		
	};

	$.fn[ pluginName ] = function (options) {
		this.each(function () {
			if (!$.data(this, "plugin_" + pluginName)) {
				$.data(this, "plugin_" + pluginName, new Plugin(this, options));
			}
			else if ($.isFunction(Plugin.prototype[options])) {
				$.data(this, 'plugin_' + pluginName)[options]();
		    }
		});

		return this;
	};

})(jQuery, window, document);
