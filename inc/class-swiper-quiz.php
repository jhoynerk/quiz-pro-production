<?php
/**
 * Override parent 'WP_Quiz_Pro' class with swiper quiz specific markup,
 * 
 */
class WP_Quiz_Pro_Swiper_Quiz extends WP_Quiz_Pro {
	
	public function get_html_questions(){
		
		$questionsHTML 	= '';
	
		if( !empty( $this->questions ) ){	
			if( $this->settings[ 'rand_questions' ] ){
				shuffle( $this->questions );
			}
			
			$height = 'auto';
			$width = '100%;';
			if( isset( $this->settings[ 'size' ] ) && $this->settings[ 'size' ] == 'custom' ) {
				if( isset( $this->settings[ 'custom_height' ] ) && !empty( $this->settings[ 'custom_height' ] ) ){
					$height = $this->settings[ 'custom_height' ]+31 . 'px;';
				}
				if( isset( $this->settings[ 'custom_width' ] ) && !empty( $this->settings[ 'custom_width' ] ) ){
					$width = $this->settings[ 'custom_width' ]+12 . 'px;';
				}
				$questionsHTML .= '<style>@media screen and (max-width:' . $this->settings[ 'custom_width' ] . 'px){.wq_QuestionWrapperSwiper > div:first-child{width:100%!important;height:auto!important}}</style>';
			}
			$style = 'width:' . $width . 'height:'.  $height;
			$questionsHTML .= $this->preview_markup();
			$questionsHTML .= '<div class="wq_QuestionWrapperSwiper" style="display:none;"><div style="margin:0 auto;'.$style.'"><div class="wq_IsSwiper"><ul>';
			
			foreach( $this->questions as $key => $question ){
				$index_count = $key + 1 . '/' . count( $this->questions );
				$questionsHTML .='
					<li class="in-deck" data-slideid="'. $question[ 'uid' ] .'">
						<i class="sprite sprite-check"></i>
						<i class="sprite sprite-times"></i>
						<div  style="height:100%;">	
							<div class="wq_questionImage"><img class="img" src="'.$question[ 'image' ].'"/><span>'.$question[ 'imageCredit' ].'</span></div>
							<div class="slide_info"><span class="slide_title" style="color:'.$this->settings[ 'font_color' ].'">'. $question[ 'title' ] . '</span><span class="slide_index">' . $index_count . '</span></div>
						</div>
					</li>';
			}
			$questionsHTML .= '</ul></div></div>';
		}

		/*$questionsHTML .= '<div class="actions"></div>';*/

		$questionsHTML .= '<div class="actions"><a href="#" class="dislike"><i class="sprite sprite-thumbs-down"></i></a>  <a href="#" class="like"><i class="sprite sprite-thumbs-up"></i></a></div></div>';

		return $questionsHTML;
	}
	
	public function preview_markup(){
		
		$html = '
			<div class="wq_swiperQuizPreviewInfoCtr">
				<p>
					' . __( '', 'wp-quiz-pro' ) . '
				</p>
				<button type="button" class="wq_beginQuizSwiperCtr" style="background-color:'.$this->settings[ 'bar_color' ].'"> ' . __( 'Begin!', 'wp-quiz-pro' ). '</button>
			</div>
		';
		return $html;
	}
	
	public function get_html_results(){
		
		$resultsHTML = '';
                $post = get_post();

		$str_to_remove = __( 'Share your Results :', 'wp-quiz-pro' );

		//$shareHTML = str_replace( $str_to_remove, '', $this->get_html_share() );
		//$shareHTML = str_replace( 'Share your Results :', '', $this->get_html_share() );
		$resultsHTML .= '<div class="wq_singleResultWrapper" style="display:none;"><div><h3>'.__( '', 'wp-quiz-pro' ).'</h3><div class="resultList" >';
		
		/*Post patrocinado, se necesita que salga este texto a penas se termine el quiz*/
		if($post->ID == 29617){
			$resultsHTML .= '<h2>¡Listo!</h2>
<h2>¿Te imaginas poder vivir todas estas satisfactorias situaciones?</h2>
<h2>Como estas,hay pocas sensaciones tan placenteras que te llenan de felicidad. Es como ponerte calcetines nuevos, o sentir por primera vez los <a href="https://swoo.sh/2oM6S7o" target="_blank" rel="noopener noreferrer">Nike React</a>, que son <span class="s1">algo difícil de explicar, pero fácil de amar.</span></h2>
<h2><a href="https://swoo.sh/2I3VSuc" target="_blank" rel="noopener noreferrer">Regístrate aquí</a><span class="s1"> y vive la experiencia de Nike React en House of Go.</span></h2>';
			
		}

		/*if( !empty( $this->questions ) ){
			usort($this->questions, function($a, $b) {
				return $b['votesUp'] - $a['votesUp'];
			});
			foreach( $this->questions as $key => $slide ){
				$position = $key + 1;
				$resultsHTML .= '
					<div id="result-'.$slide[ 'uid' ].'" class="resultItem" data-uid="' . $slide[ 'uid' ] . '" data-result="">
						<div class="resultImageWrapper">
							<img src="' . $slide[ 'image' ] . '" />
							<span class="indexWrapper">' . $position . '</span>
						</div>
						<div class="resultContent">
							<span>' . $slide[ 'title' ] . ' </span>
							<div class="resultUpDownVote">
								<span class="resultUpVote">
									<i class="fa fa-check"></i><span id="upVote">' . $this->format_number( $slide[ 'votesUp' ] ) . '</span>
								</span>
								<span class="resultDownVote">
									<i class="fa fa-times"></i><span id="downVote">' . $this->format_number( $slide[ 'votesDown' ] ) . '</span>
								</span>
							</div>
						</div>
					</div>
				';
			}
		}*/
		$resultsHTML .= '</div><div class="wq_retakeSwiperWrapper"><button style="display:none;" class="wq_retakeSwiperBtn"><i class="fa fa-undo"></i>&nbsp;'.__( 'Play Again!', 'wp-quiz-pro' ).'</button></div></div>' . $shareHTML .'</div>';
		return $resultsHTML;
		
	}

	public function format_number( $number ) {
		
		return $number >= 1000 ? round( $number/1000, 1 ). "k" : $number;
		
	}
}